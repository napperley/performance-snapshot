group = "org.digieng"
version = "0.1"

plugins {
    kotlin("multiplatform") version "1.6.21"
}

repositories {
    mavenCentral()
}

kotlin {
    val binaryName = "perf_snap"
    val mainEntryPoint = "org.digieng.perfSnap.main"

    linuxX64 {
        compilations.getByName("main") {
            cinterops.create("libssh") {
                includeDirs("/usr/include", "/usr/include/libssh")
            }
            dependencies {
                val coroutinesVer = "1.6.1"
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVer")
            }
        }
        binaries {
            executable(binaryName) {
                val programArgs = listOf("java").joinToString(separator = "")
                runTask?.args(programArgs)
                entryPoint = mainEntryPoint
            }
        }
    }
    linuxArm32Hfp("linuxArm32") {
        binaries {
            executable(binaryName) {
                entryPoint = mainEntryPoint
            }
        }
    }

    sourceSets {
        commonMain {
            dependencies {
                val kotlinVer = "1.6.21"
                implementation(kotlin("stdlib", kotlinVer))
            }
        }
    }
}
