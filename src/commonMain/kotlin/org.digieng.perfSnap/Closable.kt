package org.digieng.perfSnap

interface Closable {
    fun close()
}
