package org.digieng.perfSnap

class IoException(msg: String) : Exception(msg)
