package org.digieng.perfSnap

/**
 * Provides disk usage meta-data for a process.
 * @param binarySize Size of the binary in bytes.
 */
data class DiskUsage(val binarySize: ULong)

internal expect fun Process.fetchDiskUsage(): DiskUsage

internal expect fun Process.fetchDiskUsageRemotely(sshHost: String): DiskUsage
