package org.digieng.perfSnap.fileManagement

/** Base interface for representing a plain file, which is either a text or binary file. */
interface PlainFile : File {
    /** If *true* then this file is a binary file, however if *false* then this file is a text file. */
    val isBinaryFile: Boolean

    /**
     * Reads all text from this file.
     * @return A Pair containing the following:
     * 1. success - Will be *true* if the operation was successful.
     * 2. data - Contains all text read from the file. Each element represents a line.
     */
    suspend fun readAllText(): Pair<Boolean, Array<String>>

    /**
     * Reads some text from this file.
     * @param size How much text to read in bytes.
     * @return A Pair containing the following:
     * 1. success - Will be *true* if the operation was successful.
     * 2. data - Contains some text read from the file.
     */
    suspend fun readText(size: ULong): Pair<Boolean, String>

    /**
     * Reads all binary data from this file.
     * @return A Pair containing the following:
     * 1. success - Will be *true* if the operation was successful.
     * 2. data - Contains all binary data read from the file.
     */
    suspend fun readAllBinary(): Pair<Boolean, Array<Byte>>

    /**
     * Reads some binary data from this file.
     * @param size How much binary data to read in bytes.
     * @return A Pair containing the following:
     * 1. success - Will be *true* if the operation was successful.
     * 2. data - Contains some binary data read from the file.
     */
    suspend fun readBinary(size: ULong): Pair<Boolean, Array<Byte>>

    /**
     * Writes some text to this file.
     * @param data The text to write.
     * @return A value of *true* if the operation was successful.
     */
    suspend fun writeText(vararg data: String): Boolean

    /**
     * Appends some text to this file.
     * @param data The text to append.
     * @return A value of *true* if the operation was successful.
     */
    suspend fun appendText(vararg data: String): Boolean

    /**
     * Writes some [binary data][data] to this file.
     * @param data The binary data to write.
     * @return A value of *true* if the operation was successful.
     */
    suspend fun writeBinary(vararg data: Byte): Boolean

    /**
     * Appends some [binary data][data] to this file.
     * @param data The binary data to append.
     * @return A value of *true* if the operation was successful.
     */
    suspend fun appendBinary(vararg data: Byte): Boolean
}
