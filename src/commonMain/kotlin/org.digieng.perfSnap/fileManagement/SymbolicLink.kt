package org.digieng.perfSnap.fileManagement

/** Base interface that represents a symbolic link (is a shortcut to a file which might not exist). */
interface SymbolicLink : File {
    /** The destination file (an absolute path) that [SymbolicLink] points to. */
    val target: String
}
