package org.digieng.perfSnap.fileManagement.authorization

enum class FilePermissionType(public val num: UInt) {
    NONE(0u),
    EXEC(1u),
    WRITE(2u),
    EXEC_WRITE(3u),
    READ(4u),
    READ_EXEC(5u),
    READ_WRITE(6u),
    READ_WRITE_EXEC(7u)
}
