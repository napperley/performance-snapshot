package org.digieng.perfSnap.fileManagement.authorization

import org.digieng.perfSnap.convertOctalToDecimal

/** Provides file permission meta-data. Covers permissions for owner, group, and other. */
data class FilePermissions(
    val owner: FilePermissionType,
    val group: FilePermissionType,
    val other: FilePermissionType
)

/**
 * Generates a file mode from [FilePermissions].
 * @return The file mode in decimal form.
 */
fun FilePermissions.generateMode(): UInt =
    convertOctalToDecimal("${owner.num}${group.num}${other.num}".toInt()).toUInt()
