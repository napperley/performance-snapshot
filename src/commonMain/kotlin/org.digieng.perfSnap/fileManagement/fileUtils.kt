package org.digieng.perfSnap.fileManagement

import org.digieng.perfSnap.fileManagement.authorization.FilePermissionType
import org.digieng.perfSnap.fileManagement.authorization.FilePermissions

internal fun createFilePath(parentDirPath: String, name: String) =
    "${if (parentDirPath.isNotEmpty()) "$parentDirPath/" else ""}$name"

internal fun defaultFilePermissions() = FilePermissions(
    owner = FilePermissionType.READ_WRITE,
    group = FilePermissionType.READ_WRITE,
    other = FilePermissionType.NONE
)

internal fun defaultDirectoryPermissions() = FilePermissions(
    owner = FilePermissionType.READ_WRITE_EXEC,
    group = FilePermissionType.READ_WRITE_EXEC,
    other = FilePermissionType.NONE
)

internal fun extractFileName(filePath: String): String {
    require(filePath.isNotEmpty()) { "The file path cannot be empty" }
    val startPos = filePath.lastIndexOf("/") + 1
    val endPos = filePath.length - 1
    return filePath.slice(startPos..endPos)
}

internal fun extractParentDirectoryPath(filePath: String): String {
    require(filePath.isNotEmpty()) { "The file path cannot be empty" }
    return filePath.replace("/${extractFileName(filePath)}", "")
}