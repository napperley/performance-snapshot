package org.digieng.perfSnap

/**
 * Contains meta-data on memory usage for a process.
 * @param totalReserved Total amount of reserved RAM.
 * @param threadsUsed Number of threads used.
 */
data class MemoryUsage(val totalReserved: ULong, val threadsUsed: UInt)

internal expect fun Process.fetchMemoryUsage(): MemoryUsage

internal expect fun Process.fetchMemoryUsageRemotely(sshHost: String): MemoryUsage
