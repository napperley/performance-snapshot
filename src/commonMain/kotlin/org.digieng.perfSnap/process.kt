package org.digieng.perfSnap

/**
 * Contains meta-data on a process.
 * @param pattern The entire process name or a partial one that will result in a match.
 * @param shortName The official short name of the process.
 * @param pid Process identifier.
 */
data class Process(val pattern: String, val shortName: String, val pid: UInt, val binaryPath: String) {
    companion object {
        fun fromPattern(pattern: String) =
            if (fetchPid(pattern) == 0u) {
                throw ProcessNotFoundException()
            } else {
                val pid = fetchPid(pattern)
                Process(pattern = pattern, pid = pid, binaryPath = fetchBinaryPath(pid),
                    shortName = fetchProcessName(pid))
            }

        fun fromPatternRemotely(pattern: String, sshHost: String) =
            if (fetchPidRemotely(pattern, sshHost) == 0u) {
                throw ProcessNotFoundException()
            } else {
                val pid = fetchPidRemotely(pattern, sshHost)
                Process(pattern = pattern, pid = pid, binaryPath = fetchBinaryPathRemotely(pid, sshHost),
                    shortName = fetchProcessNameRemotely(pid, sshHost))
            }
    }
}

class ProcessNotFoundException : Exception()
