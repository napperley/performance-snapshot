package org.digieng.perfSnap.ssh

/** Contains SSH configuration information. */
data class SshConfiguration(
    val user: String,
    val host: String,
    val port: UInt,
    val publicKey: String = "",
    val privateKey: String = ""
)
