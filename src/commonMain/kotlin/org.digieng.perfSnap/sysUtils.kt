package org.digieng.perfSnap

import kotlinx.cinterop.CPointer
import platform.posix.FILE

internal expect fun fetchPid(pattern: String): UInt

internal expect fun fetchPidRemotely(pattern: String, sshHost: String): UInt

internal expect fun fetchProcessName(pid: UInt): String

internal expect fun fetchProcessNameRemotely(pid: UInt, sshHost: String): String

internal expect fun fetchBinaryPath(pid: UInt): String

internal expect fun fetchBinaryPathRemotely(pid: UInt, sshHost: String): String

internal expect fun CPointer<FILE>.writeLine(txt: String)
