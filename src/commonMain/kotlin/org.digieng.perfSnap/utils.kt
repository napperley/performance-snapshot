package org.digieng.perfSnap

import org.digieng.perfSnap.report.ConsoleReport
import org.digieng.perfSnap.report.RemoteConsoleReport
import org.digieng.perfSnap.report.TextFileReport
import kotlin.math.pow
import kotlin.system.exitProcess

private const val EXTRA_ARG_POS = 1
private const val SSH_HOST_POS = 2
private const val PROCESS_POS = 0

internal fun convertOctalToDecimal(octal: Int): Int {
    var tmp = octal
    var decimalNumber = 0
    var i = 0
    while (tmp != 0) {
        decimalNumber += (tmp % 10 * 8.0.pow(i.toDouble()).toInt())
        ++i
        tmp /= 10
    }
    return decimalNumber
}

internal val Boolean.intValue: Int
    get() = if (this) 1 else 0

internal fun startProgram(args: Array<String>) {
    checkProgramArgs(args)
    when {
        args.size == 3 && args[EXTRA_ARG_POS].startsWith("--useSsh") -> {
            val process = createRemoteProcess(args[PROCESS_POS], args[SSH_HOST_POS])
            println("Creating SSH Console report...")
            RemoteConsoleReport(process, args[SSH_HOST_POS]).createResourceUsageSnapshot()
        }
        args.size == 2 && args[EXTRA_ARG_POS].startsWith("--textFileReport=") -> {
            val process = createProcess(args[PROCESS_POS])
            println("Creating Text File report...")
            TextFileReport(process, extractTextFilePath(args)).createResourceUsageSnapshot()
        }
        else -> {
            val process = createProcess(args[PROCESS_POS])
            println("Creating Console report...")
            ConsoleReport(process).createResourceUsageSnapshot()
        }
    }
}

internal fun printProgramUsage() {
    println(
        """
            -- Performance Snapshot Usage --
            
            perf_snap process_name
             e.g. perf_snap java 
        """.trimIndent()
    )
}

internal fun createRemoteProcess(name: String, sshHost: String) = try {
    Process.fromPatternRemotely(name, sshHost)
} catch (ex: ProcessNotFoundException) {
    println("Process $name not found!")
    exitProcess(-1)
}

internal fun createProcess(name: String) = try {
    Process.fromPattern(name)
} catch (ex: ProcessNotFoundException) {
    println("Process $name not found!")
    exitProcess(-1)
}

internal fun checkProgramArgs(args: Array<String>) {
    if (args.isEmpty()) {
        printProgramUsage()
        exitProcess(-1)
    }
}

internal fun extractTextFilePath(args: Array<String>): String {
    var result = ""
    args.forEach { item ->
        if (item.startsWith("--textFileReport=")) {
            result = item.replace("--textFileReport=", "").trim()
        }
    }
    return result
}

internal fun ByteArray.createList() =
    map { i -> i.toInt().toChar() }.joinToString(separator = "").split("\n")
