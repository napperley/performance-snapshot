package org.digieng.perfSnap.report

import org.digieng.perfSnap.Process
import org.digieng.perfSnap.fetchDiskUsage
import org.digieng.perfSnap.fetchMemoryUsage

class ConsoleReport(override val process: Process) : Report {
    override fun createResourceUsageSnapshot() {
        println("-- ${process.pattern} Resource Usage --")
        val memUsage = process.fetchMemoryUsage()
        val diskUsage = process.fetchDiskUsage()
        println("Threads Used: ${memUsage.threadsUsed}")
        println("Total Reserved RAM (in KB): ${memUsage.totalReserved}")
        println("Binary Size (in bytes): ${diskUsage.binarySize}")
    }

    override fun createMemoryUsageSnapshot() {
        println("-- ${process.pattern} Memory Usage --")
        val memUsage = process.fetchMemoryUsage()
        println("Threads Used: ${memUsage.threadsUsed}")
        println("Total Reserved RAM (in KB): ${memUsage.totalReserved}")
    }

    override fun createDiskUsageSnapshot() {
        println("-- ${process.pattern} Disk Usage --")
        val diskUsage = process.fetchDiskUsage()
        println("Binary Size (in bytes): ${diskUsage.binarySize}")
    }
}
