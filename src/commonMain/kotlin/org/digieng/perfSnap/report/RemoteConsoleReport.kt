package org.digieng.perfSnap.report

import org.digieng.perfSnap.Process

expect class RemoteConsoleReport(process: Process, sshHost: String) : Report
