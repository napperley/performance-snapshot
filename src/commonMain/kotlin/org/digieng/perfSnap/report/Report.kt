package org.digieng.perfSnap.report

import org.digieng.perfSnap.Process

interface Report {
    val process: Process

    fun createResourceUsageSnapshot()

    fun createMemoryUsageSnapshot()

    fun createDiskUsageSnapshot()
}
