package org.digieng.perfSnap.report

import org.digieng.perfSnap.Process
import org.digieng.perfSnap.fetchDiskUsage
import org.digieng.perfSnap.fetchMemoryUsage
import org.digieng.perfSnap.writeLine
import platform.posix.fclose
import platform.posix.fopen

class TextFileReport(override val process: Process, val filePath: String) : Report {
    override fun createResourceUsageSnapshot() {
        val file = fopen(filePath, "w")
        if (file != null) {
            file.writeLine("-- ${process.pattern} Resource Usage --")
            val memUsage = process.fetchMemoryUsage()
            val diskUsage = process.fetchDiskUsage()
            file.writeLine("Threads Used: ${memUsage.threadsUsed}")
            file.writeLine("Total Reserved RAM (in KB): ${memUsage.totalReserved}")
            file.writeLine("Binary Size (in bytes): ${diskUsage.binarySize}")
            fclose(file)
        }
    }

    override fun createMemoryUsageSnapshot() {
        val file = fopen(filePath, "w")
        if (file != null) {
            file.writeLine("-- ${process.pattern} Resource Usage --")
            val memUsage = process.fetchMemoryUsage()
            file.writeLine("Threads Used: ${memUsage.threadsUsed}")
            file.writeLine("Total Reserved RAM (in KB): ${memUsage.totalReserved}")
            fclose(file)
        }
    }

    override fun createDiskUsageSnapshot() {
        val file = fopen(filePath, "w")
        if (file != null) {
            file.writeLine("-- ${process.pattern} Resource Usage --")
            val diskUsage = process.fetchDiskUsage()
            file.writeLine("Binary Size (in bytes): ${diskUsage.binarySize}")
            fclose(file)
        }
    }
}
