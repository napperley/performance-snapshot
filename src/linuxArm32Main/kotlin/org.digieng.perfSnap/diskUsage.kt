package org.digieng.perfSnap

import kotlinx.cinterop.alloc
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.ptr
import platform.posix.stat

internal actual fun Process.fetchDiskUsage(): DiskUsage = memScoped {
    val buf = alloc<stat>()
    stat(binaryPath, buf.ptr)
    DiskUsage(buf.st_size.toULong())
}

internal actual fun Process.fetchDiskUsageRemotely(sshHost: String): DiskUsage =
    TODO("Fetch disk usage remotely")
