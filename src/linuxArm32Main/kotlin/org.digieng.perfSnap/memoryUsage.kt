package org.digieng.perfSnap

import kotlinx.cinterop.*
import platform.posix.fclose
import platform.posix.fopen
import platform.posix.getline

internal actual fun Process.fetchMemoryUsageRemotely(sshHost: String): MemoryUsage =
    TODO("Obtain memory usage for a process via SFTP")

internal actual fun Process.fetchMemoryUsage(): MemoryUsage = memScoped {
    var bytesRead: Int
    val num = alloc<UIntVar>()
    val line = alloc<CPointerVar<ByteVar>>()
    var threadsUsed = 0u
    var totalReserved = 0uL
    val file = fopen("/proc/$pid/status", "r")
    do {
        bytesRead = getline(__n = num.ptr, __stream = file, __lineptr = line.ptr)
        val tmp = (line.value?.toKString() ?: "").trim()
        if ("Threads:" in tmp) threadsUsed = extractThreadsUsed(tmp)
        if ("VmRSS:" in tmp) totalReserved = extractTotalReserved(tmp)
    } while (bytesRead > 0)
    fclose(file)
    MemoryUsage(totalReserved, threadsUsed)
}

private fun extractThreadsUsed(line: String) = line.replace("Threads:", "").trim().toUInt()

private fun extractTotalReserved(line: String) = line
    .replace("VmRSS:", "")
    .replace("kB", "")
    .trim()
    .toULong()
