package org.digieng.perfSnap.report

import org.digieng.perfSnap.Process

actual class RemoteConsoleReport actual constructor(override val process: Process, sshHost: String) : Report {
    override fun createResourceUsageSnapshot() = TODO("Create resource usage snapshot")

    override fun createMemoryUsageSnapshot() = TODO("Create memory usage snapshot")

    override fun createDiskUsageSnapshot() = TODO("Create disk usage snapshot")
}
