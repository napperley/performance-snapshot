package org.digieng.perfSnap

import kotlinx.cinterop.*
import platform.posix.*

internal actual fun fetchProcessNameRemotely(pid: UInt, sshHost: String): String =
    TODO("Fetch process name remotely")

internal actual fun fetchPidRemotely(pattern: String, sshHost: String): UInt = TODO("Remotely fetch PID")

internal actual fun fetchBinaryPathRemotely(pid: UInt, sshHost: String): String =
    TODO("Remotely fetch binary path")

internal actual fun CPointer<FILE>.writeLine(txt: String) {
    val tmp = "$txt\n"
    fwrite(__s = this, __n = 1, __ptr = tmp.cstr, __size = tmp.length.toUInt())
}

internal actual fun fetchBinaryPath(pid: UInt): String = memScoped {
    val buf = allocArray<ByteVar>(255)
    readlink(__len = 255u, __path = "/proc/$pid/exe", __buf = buf)
    buf.toKString()
}

internal actual fun fetchPid(pattern: String): UInt = memScoped {
    val line = alloc<CPointerVar<ByteVar>>()
    val num = alloc<UIntVar>()
    val file = popen("pgrep $pattern", "r")
    getline(__n = num.ptr, __stream = file, __lineptr = line.ptr)
    val pid = try {
        (line.value?.toKString() ?: "0").trim().toUInt()
    } catch (ex: NumberFormatException) {
        0u
    }
    pclose(file)
    pid
}

internal actual fun fetchProcessName(pid: UInt): String = memScoped {
    val file = fopen("/proc/$pid/status", "r")
    val line = alloc<CPointerVar<ByteVar>>()
    val num = alloc<UIntVar>()
    getline(__n = num.ptr, __stream = file, __lineptr = line.ptr)
    val processName = (line.value?.toKString() ?: "").replace("Name:", "").trim()
    fclose(file)
    processName
}
