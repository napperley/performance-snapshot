package org.digieng.perfSnap

import kotlinx.cinterop.alloc
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.ptr
import kotlinx.coroutines.runBlocking
import org.digieng.perfSnap.fileManagement.extractFileName
import org.digieng.perfSnap.fileManagement.extractParentDirectoryPath
import org.digieng.perfSnap.sftp.RemotePlainFile
import org.digieng.perfSnap.ssh.SshClient
import platform.posix.stat

internal actual fun Process.fetchDiskUsage(): DiskUsage = memScoped {
    val buf = alloc<stat>()
    stat(binaryPath, buf.ptr)
    DiskUsage(buf.st_size.toULong())
}

internal actual fun Process.fetchDiskUsageRemotely(sshHost: String): DiskUsage {
    var result: DiskUsage
    val fileName = extractFileName(binaryPath)
    val parentDirPath = extractParentDirectoryPath(binaryPath)
    val sshClient = SshClient(host = sshHost)
    sshClient.connect()
    runBlocking {
        val binarySize = RemotePlainFile(sshClient = sshClient, parentDirPath = parentDirPath, name = fileName)
            .size()
            .toULong()
        result = DiskUsage(binarySize)
    }
    sshClient.close()
    return result
}
