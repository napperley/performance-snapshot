package org.digieng.perfSnap

import kotlinx.cinterop.*
import kotlinx.coroutines.runBlocking
import org.digieng.perfSnap.sftp.RemotePlainFile
import org.digieng.perfSnap.ssh.SshClient
import platform.posix.fclose
import platform.posix.fopen
import platform.posix.getline

internal actual fun Process.fetchMemoryUsageRemotely(sshHost: String): MemoryUsage {
    var result = MemoryUsage(totalReserved = 0uL, threadsUsed = 0u)
    val sshClient = SshClient(host = sshHost)
    sshClient.connect()
    val file = RemotePlainFile(sshClient = sshClient, parentDirPath = "/proc/$pid", name = "status")
    runBlocking {
        var threadsUsed = 0u
        var totalReserved = 0uL
        val (success, data) = file.readAllText()
        if (success) {
            data.forEach { item ->
                if ("Threads:" in item) threadsUsed = extractThreadsUsed(item)
                else if ("VmRSS:" in item) totalReserved = extractTotalReserved(item)
            }
            result = MemoryUsage(totalReserved = totalReserved, threadsUsed = threadsUsed)
        }
    }
    sshClient.close()
    return result
}

internal actual fun Process.fetchMemoryUsage(): MemoryUsage = memScoped {
    var bytesRead: Long
    val num = alloc<ULongVar>()
    val line = alloc<CPointerVar<ByteVar>>()
    var threadsUsed = 0u
    var totalReserved = 0uL
    val file = fopen("/proc/$pid/status", "r")
    do {
        bytesRead = getline(__n = num.ptr, __stream = file, __lineptr = line.ptr)
        val tmp = (line.value?.toKString() ?: "").trim()
        if ("Threads:" in tmp) threadsUsed = extractThreadsUsed(tmp)
        else if ("VmRSS:" in tmp) totalReserved = extractTotalReserved(tmp)
    } while (bytesRead > 0L)
    fclose(file)
    MemoryUsage(totalReserved, threadsUsed)
}

private fun extractThreadsUsed(line: String) = line.replace("Threads:", "").trim().toUInt()

private fun extractTotalReserved(line: String) = line
    .replace("VmRSS:", "")
    .replace("kB", "")
    .trim()
    .toULong()
