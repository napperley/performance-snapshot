package org.digieng.perfSnap.report

import org.digieng.perfSnap.Process
import org.digieng.perfSnap.fetchDiskUsageRemotely
import org.digieng.perfSnap.fetchMemoryUsageRemotely

actual class RemoteConsoleReport actual constructor(override val process: Process, val sshHost: String) : Report {
    override fun createResourceUsageSnapshot() {
        println("-- ${process.pattern} Resource Usage --")
        println("Short Process Name: ${process.shortName}")
        println("Binary Path: ${process.binaryPath}")
        val memUsage = process.fetchMemoryUsageRemotely(sshHost)
        val diskUsage = process.fetchDiskUsageRemotely(sshHost)
        println("Threads Used: ${memUsage.threadsUsed}")
        println("Total Reserved RAM (in KB): ${memUsage.totalReserved}")
        println("Binary Size (in bytes): ${diskUsage.binarySize}")
    }

    override fun createMemoryUsageSnapshot() {
        println("-- ${process.pattern} Memory Usage --")
        val memUsage = process.fetchMemoryUsageRemotely(sshHost)
        println("Threads Used: ${memUsage.threadsUsed}")
        println("Total Reserved RAM (in KB): ${memUsage.totalReserved}")
    }

    override fun createDiskUsageSnapshot() {
        println("-- ${process.pattern} Disk Usage --")
        val diskUsage = process.fetchDiskUsageRemotely(sshHost)
        println("Binary Size (in bytes): ${diskUsage.binarySize}")
    }
}
