package org.digieng.perfSnap.sftp

import kotlinx.cinterop.pointed
import kotlinx.cinterop.toKString
import libssh.*
import org.digieng.perfSnap.IoException
import org.digieng.perfSnap.fileManagement.File
import org.digieng.perfSnap.fileManagement.SymbolicLink
import org.digieng.perfSnap.fileManagement.authorization.FilePermissions
import org.digieng.perfSnap.fileManagement.authorization.generateMode
import org.digieng.perfSnap.fileManagement.createFilePath
import org.digieng.perfSnap.fileManagement.defaultFilePermissions
import org.digieng.perfSnap.ssh.SshClient
import platform.posix.O_CREAT
import platform.posix.O_EXCL

class RemoteSymbolicLink constructor(
    private val sshClient: SshClient,
    override val parentDirPath: String,
    override val name: String
) : SymbolicLink {
    override val target: String
        get() = sftp_readlink(sshClient.sftpSession, createFilePath(parentDirPath, name))?.toKString() ?: ""

    override suspend fun isValid(): Boolean {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        return try {
            val file = openSftpFile(
                filePath = createFilePath(parentDirPath, name),
                sftpSession = sshClient.sftpSession
            )
            sftp_close(file)
            true
        } catch (ex: IoException) {
            false
        }
    }

    override suspend fun size(): Long {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        var result = -1L
        val attributes = sftp_stat(sshClient.sftpSession, createFilePath(parentDirPath, name))
        if (isValid()) result = attributes?.pointed?.size?.toLong() ?: -1L
        sftp_attributes_free(attributes)
        return result
    }

    override suspend fun changePermissions(permissions: FilePermissions): Boolean {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        return sftp_chmod(
            sftp = sshClient.sftpSession, file = createFilePath(parentDirPath, name),
            mode = permissions.generateMode()
        ) == SSH_OK
    }

    override suspend fun copy(newFile: File): Boolean {
        TODO("Not yet implemented")
    }

    override suspend fun create(): Boolean {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        return try {
            val file = openSftpFile(
                filePath = createFilePath(parentDirPath, name),
                sftpSession = sshClient.sftpSession,
                accessType = O_EXCL or O_CREAT
            )
            sftp_close(file)
            changePermissions(defaultFilePermissions())
            true
        } catch (ex: IoException) {
            false
        }
    }

    override suspend fun delete(): Boolean {
        TODO("Not yet implemented")
    }

    override suspend fun fetchGroup(): String {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        var result = ""
        val attributes = sftp_stat(sshClient.sftpSession, createFilePath(parentDirPath, name))
        if (isValid()) result = attributes?.pointed?.group?.toKString() ?: ""
        sftp_attributes_free(attributes)
        return result
    }

    override suspend fun fetchUser(): String {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        var result = ""
        val attributes = sftp_stat(sshClient.sftpSession, createFilePath(parentDirPath, name))
        if (isValid()) result = attributes?.pointed?.owner?.toKString() ?: ""
        sftp_attributes_free(attributes)
        return result
    }

    override suspend fun move(newFile: File): Boolean {
        TODO("Not yet implemented")
    }

    override suspend fun changeUser(user: String): Boolean {
        TODO("Not yet implemented")
    }

    override suspend fun fetchUserId(): Int {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        var result = -1
        val attributes = sftp_stat(sshClient.sftpSession, createFilePath(parentDirPath, name))
        if (isValid()) result = attributes?.pointed?.uid?.toInt() ?: -1
        sftp_attributes_free(attributes)
        return result
    }

    override suspend fun changeGroup(group: String): Boolean {
        TODO("Not yet implemented")
    }

    override suspend fun fetchGroupId(): Int {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        var result = -1
        val attributes = sftp_stat(sshClient.sftpSession, createFilePath(parentDirPath, name))
        if (isValid()) result = attributes?.pointed?.gid?.toInt() ?: -1
        sftp_attributes_free(attributes)
        return result
    }
}
