package org.digieng.perfSnap.sftp

import org.digieng.perfSnap.fileManagement.File

/** Represents a SFTP file. */
interface SftpFile : File {
    override suspend fun changeGroup(group: String): Boolean =
        throw UnsupportedOperationException("This function isn't supported")

    override suspend fun changeUser(user: String): Boolean =
        throw UnsupportedOperationException("This function isn't supported")

    /**
     * Changes the ownership of the file by changing its [userId], and [groupId].
     * @param userId The user ID to use.
     * @param groupId The group ID to use.
     * @return A value of *true* if this operation was successful.
     */
    suspend fun changeOwnership(userId: UInt, groupId: UInt): Boolean
}
