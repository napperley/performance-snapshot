package org.digieng.perfSnap.sftp

import org.digieng.perfSnap.fileManagement.PlainFile

/** Represents a SFTP text or binary file. */
interface SftpPlainFile : SftpFile, PlainFile
