package org.digieng.perfSnap.ssh

import kotlinx.cinterop.*
import libssh.*
import org.digieng.perfSnap.Closable
import org.digieng.perfSnap.IoException
import org.digieng.perfSnap.intValue
import platform.posix.getenv

class SshClient(
    val host: String,
    val port: UInt = 22u,
    val logVerbosity: SshLogVerbosity = SshLogVerbosity.NO_LOG
) : Closable {
    internal val sshSession: ssh_session?
        get() = _sshSession
    private var _sshSession: ssh_session? = createSshSession()
    internal val sftpSession: sftp_session?
        get() = _sftpSession
    private var _sftpSession: sftp_session? = null
    val isClosed: Boolean
        get() = _isClosed
    private var _isClosed = false
    val isConnected: Boolean
        get() = if (_isClosed) false else ssh_is_connected(_sshSession) == true.intValue

    private fun createSshSession() = memScoped {
        val result = ssh_new() ?: throw IoException("Cannot create new SSH session")
        val tmpPort = alloc<IntVar>()
        val verbosity = alloc<IntVar>()
        tmpPort.value = port.toInt()
        verbosity.value = logVerbosity.value.toInt()
        ssh_options_set(session = result, type = ssh_options_e.SSH_OPTIONS_HOST, value = host.cstr)
        ssh_options_set(session = result, type = ssh_options_e.SSH_OPTIONS_PORT, value = tmpPort.ptr)
        ssh_options_set(session = result, type = ssh_options_e.SSH_OPTIONS_LOG_VERBOSITY, value = verbosity.ptr)
        result
    }

    override fun close() {
        if (!_isClosed) {
            disconnect()
            if (_sshSession != null) ssh_free(_sshSession)
            _sshSession = null
            _isClosed = true
        }
    }

    fun verifyKnownHost(): Unit = memScoped {
        require(!_isClosed) { "The SshClient instance must be open" }
        val sshKey = alloc<ssh_keyVar>()
        var rc = ssh_get_server_publickey(_sshSession, sshKey.ptr)
        val hash = alloc<UByteVar>()
        if (rc < 0) throw IoException("Cannot get public key from server")
        rc = ssh_get_pubkey_hash(_sshSession, cValuesOf(hash.ptr))
        ssh_key_free(sshKey.value)
        if (rc < 0) throw IoException("Cannot get hash for public key")
        checkKeyIsKnown()
    }

    private fun checkKeyIsKnown() {
        when (ssh_session_is_known_server(_sshSession)) {
            SSH_KNOWN_HOSTS_OK -> Unit
            else -> throw IllegalStateException("Unknown  key")
        }
    }

    fun connect(user: String = "", publicKey: String = "", privateKey: String = "") {
        require(!_isClosed) { "The SshClient instance must be open" }
        val rc = ssh_connect(_sshSession)
        if (rc != SSH_OK) {
            throw IoException("Cannot connect to SSH server: ${ssh_get_error(_sshSession)?.toKString()}")
        }
        verifyKnownHost()
        if (user.isNotEmpty() && publicKey.isNotEmpty() && privateKey.isNotEmpty()) {
            authenticateManually(user = user, publicKey = publicKey, privateKey = privateKey)
        } else {
            authenticateAutomatically()
        }
    }

    private fun authenticateAutomatically() = memScoped {
        require(!_isClosed) { "The SshClient instance must be open" }
        when (ssh_userauth_autopubkey(_sshSession, null)) {
            SSH_AUTH_ERROR -> handleAuthenticationFailure(_sshSession, "unknown error")
            SSH_AUTH_DENIED -> handleAuthenticationFailure(_sshSession, "no  key matches")
            SSH_AUTH_PARTIAL -> handleAuthenticationFailure(_sshSession, "additional authentication required")
            SSH_AUTH_SUCCESS -> Unit
        }
        setupSftpSession()
    }

    private fun authenticateManually(
        user: String,
        publicKey: String,
        privateKey: String
    ) = memScoped {
        require(!_isClosed) { "The SshClient instance must be open" }
        val homeDir = getenv("HOME")?.toKString() ?: ""
        val tmpKey = alloc<ssh_keyVar>()
        val tmpPrivateKey = alloc<ssh_keyVar>()
        ssh_pki_import_pubkey_file("$homeDir/.ssh/$publicKey", tmpKey.ptr)
        ssh_userauth_try_publickey(session = _sshSession, username = user, pubkey = tmpKey.value)
        ssh_pki_import_privkey_file(
            filename = "$homeDir/.ssh/$privateKey",
            passphrase = null,
            auth_fn = null,
            auth_data = null,
            pkey = tmpPrivateKey.ptr
        )
        authenticateWithKey(user, tmpPrivateKey.value)
        ssh_key_free(tmpPrivateKey.value)
        ssh_key_free(tmpKey.value)
        setupSftpSession()
    }

    private fun authenticateWithKey(user: String, privateKey: ssh_key?) {
        when (ssh_userauth_publickey(session = _sshSession, username = user, privkey = privateKey)) {
            SSH_AUTH_ERROR -> handleAuthenticationFailure(_sshSession, "unknown error")
            SSH_AUTH_DENIED -> handleAuthenticationFailure(_sshSession, "no  key matches")
            SSH_AUTH_PARTIAL -> handleAuthenticationFailure(_sshSession, "additional authentication required")
            SSH_AUTH_SUCCESS -> println("User authenticated")
        }
    }

    private fun setupSftpSession() {
        _sftpSession = sftp_new(_sshSession) ?: throw IoException("Cannot create new SFTP session")
        if (sftp_init(_sftpSession) != SSH_OK) {
            throw IoException("Error initializing SFTP session (code ${sftp_get_error(_sftpSession)})")
        }
    }

    private fun handleAuthenticationFailure(sshSession: ssh_session?, info: String) {
        throw IllegalStateException("Failed to authenticate ($info): ${ssh_get_error(sshSession)?.toKString()}")
    }

    fun disconnect() {
        require(!_isClosed) { "The SshClient instance must be open" }
        if (_sftpSession != null) sftp_free(_sftpSession)
        _sftpSession = null
        ssh_disconnect(_sshSession)
    }
}
