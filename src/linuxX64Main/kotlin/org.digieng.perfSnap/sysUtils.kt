package org.digieng.perfSnap

import kotlinx.cinterop.*
import kotlinx.coroutines.runBlocking
import org.digieng.perfSnap.sftp.RemotePlainFile
import org.digieng.perfSnap.sftp.RemoteSymbolicLink
import org.digieng.perfSnap.ssh.RemoteProgramRunner
import org.digieng.perfSnap.ssh.SshClient
import platform.posix.*

internal actual fun fetchProcessNameRemotely(pid: UInt, sshHost: String): String {
    var result = ""
    val sshClient = SshClient(host = sshHost)
    sshClient.connect()
    val file = RemotePlainFile(sshClient = sshClient, parentDirPath = "/proc/$pid", name = "status")
    runBlocking {
        val (success, data) = file.readAllText()
        if (success) result = data[0].replace("Name:", "").trim()
    }
    sshClient.close()
    return result
}

internal actual fun fetchPidRemotely(pattern: String, sshHost: String): UInt {
    var result: UInt
    val sshClient = SshClient(host = sshHost)
    sshClient.connect()
    val runner = RemoteProgramRunner(sshClient = sshClient, programName = pattern)
    runBlocking {
        val tmp = runner.fetchProcessId()
        result = if (tmp == -1) 0u else tmp.toUInt()
    }
    sshClient.close()
    return result
}

internal actual fun CPointer<FILE>.writeLine(txt: String) {
    val tmp = "$txt\n"
    fwrite(__s = this, __n = 1, __ptr = tmp.cstr, __size = tmp.length.toULong())
}

internal actual fun fetchBinaryPath(pid: UInt): String = memScoped {
    val buf = allocArray<ByteVar>(255)
    readlink(__len = 255u, __path = "/proc/$pid/exe", __buf = buf)
    buf.toKString()
}

internal actual fun fetchPid(pattern: String): UInt = memScoped {
    val line = alloc<CPointerVar<ByteVar>>()
    val num = alloc<ULongVar>()
    val file = popen("pgrep $pattern", "r")
    getline(__n = num.ptr, __stream = file, __lineptr = line.ptr)
    val pid = try {
        (line.value?.toKString() ?: "0").trim().toUInt()
    } catch (ex: NumberFormatException) {
        0u
    }
    pclose(file)
    pid
}

internal actual fun fetchProcessName(pid: UInt): String = memScoped {
    val file = fopen("/proc/$pid/status", "r")
    val line = alloc<CPointerVar<ByteVar>>()
    val num = alloc<ULongVar>()
    getline(__n = num.ptr, __stream = file, __lineptr = line.ptr)
    val processName = (line.value?.toKString() ?: "").replace("Name:", "").trim()
    fclose(file)
    processName
}

internal actual fun fetchBinaryPathRemotely(pid: UInt, sshHost: String): String {
    val sshClient = SshClient(host = sshHost)
    sshClient.connect()
    val binaryPath = RemoteSymbolicLink(sshClient = sshClient, parentDirPath = "/proc/$pid", name = "exe").target
    sshClient.close()
    return binaryPath
}
